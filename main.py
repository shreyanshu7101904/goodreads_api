import sys
from scripts.goodreads import GoodreadsAPIClient

def help():
    msg = "book name is not provided.\nUsage python3 main.py book_title"
    return msg

if __name__ == "__main__":
    # title_url = sys.argv[1:][0]
    if sys.argv[1:]:
        title_url = sys.argv[1:][0]
        if 'help' in title_url:
            help()
        else:
            url = "https://www.goodreads.com/book/show/12177850-a-song-of-ice-and-fire"
            print(sys.argv[1:])
            ob = GoodreadsAPIClient(url)
    else:
        print(help())

