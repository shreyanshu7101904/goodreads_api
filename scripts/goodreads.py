from requests import get
import xml.etree.ElementTree as ET
import unittest
import sys
sys.path.append('../')
from scripts.config import api_key, url
# api_key = "5VI9EeBQZ7z2P7SadUXFvQ"
# url = "https://www.goodreads.com/book/show/"


class InvalidGoodreadsURL(Exception):
    """Invalid GoodRead Book URL"""
    def __init__(self, response_code=404, message=None):
        self.response_code = response_code
        if message:
            self.message = message
        else:
            self.message = "InInvalidGoodreadsURL Exception Raised" 
        
    def __str__(self):
        error_message = self.message + "With Response code = {}".format(self.response_code)
        return error_message


class GoodreadsAPIClient:
    def __init__(self, title_url):
        self.title_url = title_url
        self.books_attr = {
            "title": "title",
            "average_rating": "average_rating",
            "ratings_count": "work/reviews_count",
            "num_pages": "num_pages",
            "image_url": "image_url",
            "publication_year": "publication_year",
            "authors": "author",
        }
        self.allowed_status_code = [200]
        if  not "https://" in self.title_url:
            self.title_url = url + self.title_url + ".xml?key=" + api_key
        else:
            self.title_url = self.title_url + ".xml?key=" + api_key
    
    def response_type_converter(self, data):
        data['average_rating'] = float(data['average_rating']) if data['average_rating'] else ""
        data['ratings_count'] = int(data['ratings_count']) if data['ratings_count'] else ""
        data['num_pages'] = int(data['num_pages']) if data['num_pages'] else ""
        return data

    def response_xml_parser(self, response_data):
        books_data_dict = {}
        tree = ET.fromstring(response_data)
        book_data = tree.find('book')
        for key in self.books_attr:
            val = book_data.find(self.books_attr[key])
            if val is not None:
                books_data_dict[key] = val.text
            else:
                books_data_dict[key] = ""
            if key == 'authors':
                authors = book_data.findall(key+'/author')
                authors_name = ""
                for author in authors:
                    authors_name += author.find('name').text +","
                books_data_dict[key] = authors_name

        return self.response_type_converter(books_data_dict)

    def get_book_details(self):
        url_response = get(self.title_url)
        if url_response.status_code in self.allowed_status_code:
            book_data = self.response_xml_parser(url_response.content)
            return book_data
        else:
            raise InvalidGoodreadsURL(response_code=url_response.status_code)
    
class GoodreadsAPIClientTest(unittest.TestCase):
    """GoodReadApiClient Test Case"""

    correct_url = "https://www.goodreads.com/book/show/12177850-a-song-of-ice-and-fire"
    correct_url_ob = GoodreadsAPIClient(correct_url)
    invalid_url = "https://www.goodreads.com/book/show/testbhcsbcswoevnovn"
    correct_url_ob = GoodreadsAPIClient(invalid_url)

    def test_get_book_details(self):
        self.assertEqual(1, 1)
        

if __name__ == "__main__":
    # url = "https://www.goodreads.com/book/show/12177850-a-song-of-ice-and-fire"
    # # url = "https://www.goodreads.com/book/show/testbhcsbcswoevnovn"
    # ob = GoodreadsAPIClient(url)
    # print(ob.get_book_details())
    unittest.main()
    print("hi")