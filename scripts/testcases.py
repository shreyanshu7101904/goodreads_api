import unittest
import  sys
sys.path.append('../')
from scripts.goodreads import  GoodreadsAPIClient, InvalidGoodreadsURL

book_name = "12177850-a-song-of-ice-and-fire"
test_url_ob = "https://www.goodreads.com/book/show/12177850-a-song-of-ice-and-fire.xml?key=5VI9EeBQZ7z2P7SadUXFvQ"
correct_url = "https://www.goodreads.com/book/show/12177850-a-song-of-ice-and-fire"
invalid_url = "https://www.goodreads.com/book/show/invalid_url"


class GoodreadsAPIClientTest(unittest.TestCase):
    """GoodReadApiClient Test Case"""

    def test_get_book_url_from_name(self):
        ob = GoodreadsAPIClient(book_name)
        self.assertEqual(ob.title_url, test_url_ob, msg="urls from book name are not equal")
    
    def test_get_book_url_from_url(self):
        ob = GoodreadsAPIClient(correct_url)
        self.assertEqual(ob.title_url, test_url_ob, msg="urls from url values are not equal")

    def test_data_type(self):
        ob = GoodreadsAPIClient(book_name)
        data = ob.get_book_details()
        self.assertIsInstance(data['title'], str, msg="title datatype doesnot match")
        self.assertIsInstance(data['average_rating'], float, msg='average_rating datatype doesnot match')
        self.assertIsInstance(data['num_pages'], int, msg='num_pages datatype doesnot match')
        self.assertIsInstance(data['ratings_count'], int, msg= 'ratings_count datatype doesnot match')
        self.assertIsInstance(data['image_url'], str, msg='image_url datatype doesnot match')
        self.assertIsInstance(data['publication_year'], str, msg='publication year datatype doesnot match')
        self.assertIsInstance(data['authors'], str, msg='authors datatype doesnot match')

    def test_invalid_url_exception(self):
        ob = GoodreadsAPIClient(invalid_url)
        with self.assertRaises(InvalidGoodreadsURL):
            ob.get_book_details()


    def run_test(self):
        unittest.main()
    
if __name__ == '__main__':
    unittest.main()